#!/bin/bash

set -euo pipefail

if [ "$#" -ne 1 ]; then
    echo "Invalid number of arguments"
    echo "./generate_tag <ENV_VAR_NAME>"
    exit -1
fi

echo "GENERATING TAG WITH NAME: ${1}"
TARGET_BRANCH=""

# Merge request jobs set different variables (CI_COMMIT_BRANCH is not available)
# i.e if merge request branch is defined, use that (if not empty)
if [ ! -z "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME-""}" ]; then
    echo "Currently on branch: ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
    TARGET_BRANCH=$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
else 
    echo "Currently on branch: ${CI_COMMIT_BRANCH}"
    TARGET_BRANCH=$CI_COMMIT_BRANCH
fi

# Failsafe (if empty)
if [ -z "${TARGET_BRANCH}" ]; then
    echo "TARGET_BRANCH not found."
    exit 1
fi

# If it is the target branch and not a merge request, then it is a commit being made to master
# in which case we want to generate a release image with a special tag
# The images are tagged with <date>-SHA<commit sha short version>
if [ $TARGET_BRANCH = ${RELEASE_BRANCH-""} ]; then
    tag="$(date --utc +%F)-SHA${CI_COMMIT_SHORT_SHA-"NOTFOUND"}"
    export $1=$tag
    echo "Commit is being made to master branch via merge request"
    echo "Set environment variable ${1} as: ${tag}"
# The images on non-master branches are called branchname-latest
else
    branch_replaced=$(echo "${TARGET_BRANCH}" | sed 's|/|-|g')
    branch_replaced=$(echo "${branch_replaced}" | sed 's|:|-|g')
    branch_replaced=$(echo "${branch_replaced}" | sed s/[^[:alnum:]_-]//g )
    tag="${branch_replaced}-latest"
    export $1=$tag
    echo "Commit is being made to non-default branch"
    echo "Set environment variable ${1} as: ${tag}"
fi
## finito