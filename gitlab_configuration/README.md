# Gitlab setup
Configuring gitlab for the ci/cd process with kubernetes.

## PROBLEM: CI/CD credits
We currently only have 400CI/CD minute credits per month which will not be nearly enough.
This means we can only run pipelines for a total of 400 minutes per month. 
A pipeline could take up to 10 minutes to run on master and subsequently half of that on any other branch.

Credits can be bought, although getting gitlab premium would enable other features we could benefit from.

## TODO: Run the pipeline from the cluster
Not yet researched very thoroughly and requires gitlab premium.

https://docs.gitlab.com/runner/install/kubernetes.html

We want our gitlab to run the ci/cd pipeline using our kubernetes cluster.
This is not a requirement, but should decrease the pipeline runtime significantly.

## Register kubernetes agent on gitlab
_Why do we need to do this?_\
Registering the agent allows us to share the kubernetes credentials with gitlab safely.\
This way we can run kubectl commands in the gitlab pipeline using our clusters kubeconfig.

Example usage:
```yml
 deploy:
   image:
     name: bitnami/kubectl:latest
     entrypoint: [""]
   script:
   - kubectl config use-context hauks96/kubernetes-test:primary-agent
   - kubectl get pods
```

Read more: https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html

A video going through the agent setup process: https://www.youtube.com/watch?v=XuBpKtsgGkE&ab_channel=GitLabUnfiltered

### 1. Create a folder path in the root of the repository
>```bash
># While in repository root directory
>mkdir .gitlab
>mkdir .gitlab/agents
>mkdir .gitlab/agents/primary-agent
>```

### 2. Create an empty config.yaml file
>```bash
>touch .gitlab/agents/primary-agent/config.yaml
>``` 

With gitlab premium deployments to the cluster can be automated using this config file but we do not need or want that
since our deployment files will contain runtime configured variables.


### 3. Connect the cluster to gitlab
Go to the `infrastructure / kubernetes cluster` panel in gitlab and select `"connect existing cluster"`.\
There you can select the new agent you created earlier.

You will be prompted to run a command in your cluster instance as such:

>```bash
>docker run --pull=always --rm \
>    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
>    --agent-token=REDACTED \
>    --kas-address=wss://kas.gitlab.com \
>    --agent-version stable \
>    --namespace gitlab-kubernetes-agent | kubectl apply -f -
>```

Since we are using microk8s we need to change a few things in this command.
1. The `kubectl apply -f -` command needs to have `microk8s` in front of it: `microk8s kubectl apply -f -`
2. The command needs superuser priveledges: `sudo docker run`

Before you can run this you need to install docker on the instance.
> `sudo apt-get install docker.io`

Now we can run the command with our changes:
>```bash
>sudo docker run --pull=always --rm \
>    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
>    --agent-token=REDACTED \
>    --kas-address=wss://kas.gitlab.com \
>    --agent-version stable \
>    --namespace gitlab-kubernetes-agent | microk8s kubectl apply -f -
>```

If you refresh the gitlab kubernetes page you should now see that the agent is connected:
![kubernetes agent configuration](images/kubernetes-agent.png)

### 4. Debugging
If there are any problems you can check the logs via kubectl
>`microk8s kubectl get pods -n gitlab-kubernetes-agent`

This should output the name of the running pod:
>```
>NAME                           READY   STATUS    RESTARTS   AGE
>gitlab-agent-649ffd8bf-72fjj   1/1     Running   0          5m17s
>```

To check the logs on the pod run the following command
> `microk8s kubectl logs gitlab-agent-649ffd8bf-72fjj -n gitlab-kubernetes-agent`

This should output something like this:
>```
> {"level":"info","time":"2021-12-29T19:39:49.772Z","msg":"Feature status change","feature_name":"tunnel","feature_status":true}
> {"level":"info","time":"2021-12-29T19:39:49.773Z","msg":"Observability endpoint is up","mod_name":"observability","net_network":"tcp","net_address":"[::]:8080"}
>```

To restart the pod run the following command
> `microk8s kubectl delete pod gitlab-agent-649ffd8bf-72fjj -n gitlab-kubernetes-agent`


## Create a deploy token
In order for kubernetes to be able to pull images from our private gitlab registry
we need to give it a secret access key.

We are going to generate one via gitlab and then add it to gitlab's environment variables
in base64 format. This way we can then replace it with a temporary variable in the k8 yaml files.

1. Login to GitLab and navigate to your project
2. Navigate to `Settings -> Repository` and expand the `Deploy Tokens` section
3. In the Add a deploy token of the Deploy Tokens section: -
4. Provide a Name. This is symbolic and is just for reference
5. Provide an Expires at value if you want the token to have a life-span
6. Provide a Username. This, along with the generated token, will be used in our secret.
7. Click the read_registry scope
8. Click Create deploy token

![create deploy key](images/deploy_token.png)

The deploy token is only visible at this stage so take a copy of the Username and the Token, which is essentially the registry access password.

Now we want to generate the secret we will add to gitlab's environment variables. The following commands will create a file your_secret.txt containing the value you need. **Make sure to remove this file when you are done so that it doesn't end up in your git repository.**

```bash
cd gitlab_configuration
gitlab_user=<Username> # SET YOUR DEPLOY KEY USERNAME HERE
gitlab_token=<Token> # SET YOUR DEPLOY KEY TOKEN HERE
gitlab_email=<Email> # SET YOUR GITLAB EMAIL HERE
gitlab_secret=$(echo -n "$gitlab_user:$gitlab_token" | base64 -w 0)
cat secret_generate.json \
    | sed "s/{{USER}}/$gitlab_user/" \
    | sed "s/{{TOKEN}}/$gitlab_token/" \
    | sed "s/{{EMAIL}}/$gitlab_email/" \
    | sed "s/{{SECRET}}/$gitlab_secret/" \
    | base64 -w 0 > your_secret.txt
```

Example usage:
```
cd gitlab_configuration
gitlab_user=kube-cluster
gitlab_token=dfa3245xdfgSDFG
gitlab_email=agir96@gmail.com
gitlab_secret=$(echo -n "$gitlab_user:$gitlab_token" | base64)
cat secret_generate.json \
    | sed "s/{{USER}}/$gitlab_user/" \
    | sed "s/{{TOKEN}}/$gitlab_token/" \
    | sed "s/{{EMAIL}}/$gitlab_email/" \
    | sed "s/{{SECRET}}/$gitlab_secret/" \
    | base64 -w 0 > your_secret.txt
```

Save as variable:
1. Navigate to `Settings -> CI/CD` and expand the `variables` section
2. Create a new variable named `CI_CD_CONTAINER_REGISTRY_PULL_SECRET` and set the value to the value generated.
3. Make sure to click the `protect variable` and `mask variable` option. If it isn't an option, your base64 string is invalid.


To check if your encoding is correct you can manually create the token on the cluster under a certain namespace.
The reason why we want to add this to our gitlab configuration is because the secret is only valid in the namespace
that it is created in and we want to be able to create new namespaces on the fly.

To create a token manually on the cluster:
```bash
kubectl \
    --namespace default \
    create secret docker-registry gitlab-token-auth \
    --docker-server=https://registry.gitlab.com \
    --docker-username=kube-cluster \
    --docker-password="z12BohvbKrhCQ2qhBXS5" \
    --docker-email=agir96@gmail.com
```

To view and compare the token on base64 format:
```bash
kubectl --namespace default get secret gitlab-token-auth --output="jsonpath={.data.\.dockerconfigjson}"
```

To view the decoded auth token:
```bash
kubectl --namespace default get secret gitlab-token-auth --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode
```

To delete the auth token when you are done:
```bash
kubectl --namespace default delete secret gitlab-token-auth
```

## How to use the deploy token

Now for the future, any variables that we set in the pipeline that shall replace something in a file should have the prefix `CI_CD_`.
In the file where the variable shall be set, the name of the variable is whatever comes after `CI_CD_` or in this case `CONTAINER_REGISTRY_PULL_SECRET`.

We will later look at a script that will take care of making these replacements. For now, this is how the secret.yml file should look like for 
any application that needs to pull an image from gitlab.

```yml
kind: Secret
apiVersion: v1
metadata:
  name: gitlab-pull-secret
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: {{CONTAINER_REGISTRY_PULL_SECRET}}
```

The deployment.yml files then need to refrence the secret as such
```yml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: gitlab-image
spec:
 ...
  template:
    ...
    spec:
      containers:
      - name: website
        image: registry.gitlab.com/my-namespace/my-project:latest
        imagePullPolicy: Always
      imagePullSecrets:
      - name: gitlab-pull-secret
```

