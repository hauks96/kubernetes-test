import Config

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.

# Start the phoenix server if environment is set and running in a  release
# if System.get_env("PHX_SERVER") && System.get_env("RELEASE_NAME") do
#   config :hello, HelloWeb.Endpoint, 
#     server: true,
# end

# ------------ ENDPOINT ENVIRONMENT ------------- #
# APP_NAME              STRING            REQUIRED
# APP_PORT              INT/STRING        REQUIRED
# APP_HOST              STRING            REQUIRED
# SECRET_KEY_BASE       STRING            REQUIRED
# NAMESPACE             STRING            REQUIRED 
# POD_IP                STRING            REQUIRED


# ------------ REPO (DB) ENVIRONMENT ------------ #
# DB_HOSTNAME           STRING           REQUIRED
# DB_USERNAME           STRING           REQUIRED
# DB_PASSWORD           STRING           REQUIRED
# DB_NAME               STRING           REQUIRED
# POOL_SIZE             INT              OPTIONAL

# Making all environments use the same setup will greatly
# reduce uncertainty with releases
if config_env() == :prod do
  # Not used here but in env.sh.eex - Just checking for it's existence
  deployment_namespace = System.get_env("NAMESPACE") ||
    raise """
    environment variable NAMESPACE is missing.
    For example: hauks96-staging
    """

  pod_ip = System.get_env("POD_IP") ||
    raise """
    environment variable POD_IP is missing.
    For example: 192.168.80.34
    """

  # Used in the application
  app_name = System.get_env("APP_NAME") ||
    raise """
    environment variable APP_NAME is missing.
    For example: my-app
    """

  app_port =  String.to_integer(System.get_env("APP_PORT")) ||
    raise """
    environment variable APP_PORT is missing.
    For example: 4000
    """

  app_host = System.get_env("APP_HOST") ||
    raise """
    environment variable APP_HOST is missing.
    For example: 0.0.0.0
    """

  # The secret key base is used to sign/encrypt cookies and other secrets.
  # A default value is used in config/dev.exs and config/test.exs but you
  # want to use a different value for prod and you most likely don't want
  # to check this value into version control, so we use an environment
  # variable instead.
  secret_key_base =
    System.get_env("SECRET_KEY_BASE") ||
      raise """
      environment variable SECRET_KEY_BASE is missing.
      You can generate one by calling: mix phx.gen.secret
      Then add it to the CI pipeline variables.
      """

  config :hello, HelloWeb.Endpoint, 
    http: [port: app_port],
    url: [host: app_host, port: app_port], # Access to the app is local, the ingress takes care of making it accessable from the outside.
    server: true,
    http: [
      # Enable IPv6 and bind on all interfaces.
      # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
      # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
      # for details about using IPv6 vs IPv4 and loopback vs public addresses.
      ip: {0, 0, 0, 0, 0, 0, 0, 0}, # might have to change this to {0, 0, 0, 0}
      port: app_port
    ],
    secret_key_base: secret_key_base,
    my_name: app_name,
    is_production: true

  maybe_ipv6 = if System.get_env("ECTO_IPV6"), do: [:inet6], else: []

  db_hostname =
    System.get_env("DB_HOSTNAME") ||
      raise """
      environment variable DB_HOSTNAME is missing.
      For example: example.com 
      """
  db_username =
    System.get_env("DB_USERNAME") ||
      raise """
      environment variable DB_USERNAME is missing.
      For example: postgres
      """

  db_password =
    System.get_env("DB_PASSWORD") ||
    raise """
    environment variable DB_PASSWORD is missing.
    For example: ********
    """

  db_name =
    System.get_env("DB_NAME") ||
    raise """
    environment variable DB_NAME is missing.
    For example: device_db
    """

  config :hello, Hello.Repo,
    hostname: db_hostname,
    username: db_username,
    password: db_password,
    database: db_name,
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10"),
    socket_options: maybe_ipv6


  # ## Using releases
  #
  # If you are doing OTP releases, you need to instruct Phoenix
  # to start each relevant endpoint:
  #
  #     config :hello, HelloWeb.Endpoint, server: true
  #
  # Then you can assemble a release by calling `mix release`.
  # See `mix help release` for more information.

  # ## Configuring the mailer
  #
  # In production you need to configure the mailer to use a different adapter.
  # Also, you may need to configure the Swoosh API client of your choice if you
  # are not using SMTP. Here is an example of the configuration:
  #
  #     config :hello, Hello.Mailer,
  #       adapter: Swoosh.Adapters.Mailgun,
  #       api_key: System.get_env("MAILGUN_API_KEY"),
  #       domain: System.get_env("MAILGUN_DOMAIN")
  #
  # For this example you need include a HTTP client required by Swoosh API client.
  # Swoosh supports Hackney and Finch out of the box:
  #
  #     config :swoosh, :api_client, Swoosh.ApiClient.Hackney
  #
  # See https://hexdocs.pm/swoosh/Swoosh.html#module-installation for details.
end
