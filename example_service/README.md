# Deploying services
This example service is deploying HttpBin which is contained within a docker image

To deploy this service we run the following command in our terminal:
> `kubectl apply -f ingress.yaml -f service.yaml -f deployment.yaml`

We can check the status of the deployment:
> `kubectl get pods`

Output when ready:
```
NAME                       READY   STATUS    RESTARTS   AGE
httpbin-86956d44c4-kvzsx   1/1     Running   0          50s
```

We can check the logs on this pod:
> `kubectl logs httpbin-86956d44c4-kvzsx`

We can delete the pod to spin up a fresh one. 
Kubernetes will take care of this for us automatically as it always tries to keep the desired state.
> `kubectl delete pod httpbin-86956d44c4-kvzsx`

### Configure the yaml files
The only changes you need to do to the k8 yaml files is to **change the host url in the ingress.yaml file** to httpbin.{your-sub-domain}.{your-domain}.com


## Verify that the service is up
There should be a website up and running on `httpbin.{your-sub-domain}.{your-domain}.com`.

for example: `httpbin.iris.aegir-devops.com`